//
//  Write.swift
//  git test MC2 WatchKit Extension
//
//  Created by FrancescaCiancio on 21/01/2020.
//  Copyright © 2020 FrancescaCiancio. All rights reserved.
//

import SwiftUI

struct Write: View {
    var body: some View {
        VStack{
            Text("The functionality will be available in the next update")
                .multilineTextAlignment(.center)
        }.navigationBarTitle("cancel")
    }
}

struct Write_Previews: PreviewProvider {
    static var previews: some View {
        Write()
    }
}
