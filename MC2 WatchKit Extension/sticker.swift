//
//  sticker.swift
//  git test MC2 WatchKit Extension
//
//  Created by FrancescaCiancio on 21/01/2020.
//  Copyright © 2020 FrancescaCiancio. All rights reserved.
//

import SwiftUI

struct sticker: View {
    var body: some View {
        VStack{
            HStack{
                Image("Happy").resizable().scaledToFit()
                    .frame(width: 80.0, height: 80.0)
                
                
                Image("Gift").resizable().scaledToFit()
                    .frame(width: 80.0, height: 80.0)
            }
            Spacer()
            Spacer()
            
            HStack{
                Image("Honey").resizable().scaledToFit()
                    .frame(width: 80.0, height: 80.0)
                Image("Flower").resizable().scaledToFit()
                    .frame(width: 80.0, height: 80.0)
            }
        }.navigationBarTitle("cancel")
            .padding()
    }
}

struct sticker_Previews: PreviewProvider {
    static var previews: some View {
        sticker()
    }
}
