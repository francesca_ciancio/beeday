//
//  HostingController.swift
//  MC2 WatchKit Extension
//
//  Created by FrancescaCiancio on 09/06/2020.
//  Copyright © 2020 Francesca Ciancio. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView(model: Model())
    }
}
