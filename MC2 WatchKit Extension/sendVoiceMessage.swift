//
//  sendMessage.swift
//  git test MC2 WatchKit Extension
//
//  Created by FrancescaCiancio on 20/01/2020.
//  Copyright © 2020 FrancescaCiancio. All rights reserved.
//

import SwiftUI

struct SendVoiceMessage: View {
    var body: some View {
        VStack{
            Spacer()
            Image("voice")
                .frame(width: 100.0, height: 100.0)
//                .scaledToFit()
            Spacer()
            Image("line")
            Spacer()
            Image(systemName: "play.circle")
            
        }.navigationBarTitle("Cancel")
    }
}

struct sendMessage_Previews: PreviewProvider {
    static var previews: some View {
        SendVoiceMessage()
    }
}

