//
//  LogoView.swift
//  git test MC2 WatchKit Extension
//
//  Created by Emanuele Petrosino on 22/01/2020.
//  Copyright © 2020 FrancescaCiancio. All rights reserved.
//

import SwiftUI

struct LogoView: View {
    var body: some View {
        Image("logo")
            .resizable()
            .frame(width: 109 * 0.5 , height: 128 * 0.5)
            .clipShape(Circle())
            .aspectRatio(contentMode: .fit)}
    //
    //this is how I did it before, try not to screw it up
    //        150 x 96
    //            Image("notification")
    //        .clipShape(Rectangle())
    //        .frame(width: 150 * 0,5, height: 96 * 0,5)}
    //    the idea was to use the basis for the notification as an image and write over it. I can do it, it doesn't mean I'm able to ^_^
    
    
    
    
}

struct LogoView_Previews: PreviewProvider {
    static var previews: some View {
        LogoView()
    }
}

