//
//  ContentView.swift
//  git test MC2 WatchKit Extension
//
//  Created by FrancescaCiancio on 24/01/2020.
//  Copyright © 2020 FrancescaCiancio. All rights reserved.
//
import SwiftUI
//how every birthday looks
struct BirthView: View {
    var  birthday : Birthday
    var body: some View {
        VStack(alignment: .center) {
            ZStack {
                Rectangle().cornerRadius(10).foregroundColor(Color(#colorLiteral(red: 0.7227770686, green: 0.5868101716, blue: 0.1192140505, alpha: 1)))
                Text(birthday.date)
                    .fontWeight(.medium)
                    .foregroundColor(Color.black)
            }
            Spacer()
            Text(birthday.name + "'s birthday")
                .lineLimit(2)
            Spacer()
        }
        .padding(.horizontal, -8.0)
    }
}
//this is what will be showed
struct ContentView: View {
    @ObservedObject var model : Model
    
    var body: some View {
        
        List{
            ForEach(model.birthdays, id: \.self) { birthday in
                NavigationLink(destination: SecondView(birthday: birthday)) {
                    BirthView(birthday: birthday)
                }
                .listRowPlatterColor(Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1600115741)))
            }
            NavigationLink(destination: NotificationView()){
                Text("test notification")
            }
        }.navigationBarTitle("BeeDay")
    }
}




struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(model: Model())
    }
}
