//
//  Model.swift
//  git test MC2 WatchKit Extension
//
//  Created by FrancescaCiancio on 24/01/2020.
//  Copyright © 2020 FrancescaCiancio. All rights reserved.
//

import Foundation

class Model: ObservableObject {
    // all the birthdays
    @Published var birthdays = [Birthday(name: "Rafaela", date: "27 Jan"), Birthday(name: "Marco", date: "24 Jan"),Birthday(name: "Emanuele", date: "2 Feb"),Birthday(name: "Sandra", date: "12 Mar")]
}

//the proprieties of every birthday
struct Birthday: Hashable {
    var name: String
    var date: String
}
