//
//  NotificationView.swift
//  git test MC2 WatchKit Extension
//
//  Created by FrancescaCiancio on 20/01/2020.
//  Copyright © 2020 Emanuele Petrosino. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    @State private var showingAlert = false
    
    var body: some View {
        ZStack{
            Rectangle()
                .cornerRadius(15)
                .frame(width: 160.0, height: 45.0)
                .foregroundColor(Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1600115741)))
                .position(x: 91, y:65)
            
            
            
            
            Button(action: {
                
                self.showingAlert = true
            }) {
                Text("Show Alert")
            }
            .alert(isPresented: $showingAlert){
                
                Alert(title: Text("Bday"), message: Text("Today is someone's birthday"), primaryButton: Alert.Button.default(Text("Send"), action: {
                    
                })
                    
                    
                    , secondaryButton: Alert.Button.cancel(Text("Dismiss"), action: {
                        print("no clicked")
                    })
                )
                //   the $ before showingAlert in 20 is a Binding, now the property is binded to the Alert that follows
            }
            .background((Color(#colorLiteral(red: 0.7227770686, green: 0.5868101716, blue: 0.1192140505, alpha: 1)))
            .cornerRadius(10))
            .frame(width: 160.0, height: 45.0)
            
            LogoView()
                .position(x: 30, y: 50)
            
            Text("Beeday")
                .position(x:100, y:50)
                .font(/*@START_MENU_TOKEN@*/.footnote/*@END_MENU_TOKEN@*/)
            
            
            //this is the logo, spaced arbitrairly but hey it works
            Spacer()
            
            
            
        }
    }
    
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}

