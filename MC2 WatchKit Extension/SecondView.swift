//
//  SecondView.swift
//  git test MC2 WatchKit Extension
//
//  Created by FrancescaCiancio on 24/01/2020.
//  Copyright © 2020 FrancescaCiancio. All rights reserved.
//

import SwiftUI

struct SecondView: View {
    //   to make naviation goes in the correct view
    @State var showingDetail1 = false
    @State var showingDetail2 = false
    @State var showingDetail3 = false
    var  birthday : Birthday
    var body: some View{
        List {
            //            horizontal stack with the 3 circles
            HStack {
                Button(action: {
                    self.showingDetail1.toggle()
                }) {
                    ZStack{
                        Circle()
                            .frame(width: 45, height: 45) .cornerRadius(10)
                            .foregroundColor(Color.myYellow)
                        Image(systemName:"mic")
                            .resizable()
                            .frame(width: 26.0, height: 32.0)
                            .scaledToFit()
                    }
                }.sheet(isPresented: $showingDetail1) {
                    SendVoiceMessage()
                }
                Button(action: {
                    self.showingDetail2.toggle()
                }) {
                    //                    to add the image upon the circle
                    ZStack{
                        Circle()
                            .frame(width: 45, height: 45) .cornerRadius(10)
                            .foregroundColor(Color.myYellow)
                        Image(systemName:"smiley")
                            .resizable()
                            .frame(width: 35.0, height: 35.0)
                            .scaledToFit()
                    }
                }.sheet(isPresented: $showingDetail2) {
                    sticker()
                }
                Button(action: {
                    self.showingDetail3.toggle()
                }) {
                    ZStack{
                        Circle()
                            .frame(width: 45, height: 45) .cornerRadius(10)
                            .foregroundColor(Color.myYellow)
                        Image(systemName:"textformat.size")
                            .resizable()
                            .frame(width: 32.0, height: 24.0)
                            .scaledToFit()
                    }
                }.sheet(isPresented: $showingDetail3) {
                    Write()
                }
            }
            .listRowPlatterColor(Color.myBlack)
            .buttonStyle(PlainButtonStyle())
            
            //            the happy birthday messages
            Text("Happy Birthday")
            Text("Best Whishes!")
            Text("You're Special")
                
                .buttonStyle(PlainButtonStyle())
        }.listRowPlatterColor(Color.myGray)
            .buttonStyle(PlainButtonStyle())
            .navigationBarTitle(birthday.name)
    }
}
// MARK: -Color system
//this is an option to implement a custom color
extension Color{
    static let myYellow = Color(#colorLiteral(red: 0.7227770686, green: 0.5868101716, blue: 0.1192140505, alpha: 1))
    static let myBlack = Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0))
    static let myGray = Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1600115741))
}
struct SecondView_Previews: PreviewProvider {
    static var previews: some View {
        SecondView(birthday: Birthday(name: "for the preview", date: "date"))
    }
}
